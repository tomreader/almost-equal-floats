package com.gitlab.thomasreader.almostequalfloats

import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.sign

/**
 * The difference between 1.0f and the smallest float greater than 1.0f (```2^0 * (1 + 1 / 2^23) - 1```)
 */
inline val Float.Companion.FLT_EPSILON: Float
    get() = 1.19209290E-07F

/**
 * Delegated compound comparison operation combining equality, epsilon equality and ulp equality to determine whether
 * this and the specified float are almost equal. This provides overlapping coverage where epsilon comparisons between
 * floats can provide better equality than ulp which struggles comparing positive and negative numbers.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 *
 * @param that the comparison float
 * @param ulp the allowed difference in units in last place
 * @param epsilon the epsilon
 * @return true if this==that or either of the delegate operations return true, false otherwise
 * @see Float.equalsEpsilon
 * @see Float.equalsUlp
 */
fun Float.equalsEpsilonUlp(that: Float, ulp: Int = 1, epsilon: Float = Float.FLT_EPSILON): Boolean {
    return this == that || this.equalsEpsilon(that, epsilon) || this.equalsUlp(that, ulp)
}

/**
 * Delegated compound comparison operation combining equality, epsilon equality and relative equality to determine
 * whether this and the specified float are almost equal. This provides overlapping coverage where epsilon comparisons
 * between floats can provide better equality than relative difference with small floats around 0.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 *
 * @param that the comparison float
 * @param relDiff the relative difference
 * @param epsilon the epsilon
 * @return true if this==that or either of the delegate operations return true, false otherwise
 * @see Float.equalsEpsilon
 * @see Float.relativeDifference
 */
fun Float.equalsEpsilonRelDiff(that: Float, relDiff: Float = Float.FLT_EPSILON, epsilon: Float = Float.FLT_EPSILON): Boolean {
    return this == that || this.equalsEpsilon(that, epsilon) || this.equalsRelDifference(that, relDiff)
}

/**
 * Compares this value with the specified float to see whether they are almost equal by checking if the difference
 * is less than or equal to the epsilon.
 *
 * @param that the comparison float
 * @param epsilon the machine epsilon
 * @return true if the absolute difference <= epsilon
 */
fun Float.equalsEpsilon(that: Float, epsilon: Float = Float.FLT_EPSILON): Boolean {
    return (this - that).absoluteValue <= epsilon.absoluteValue
}

/**
 * Compares this value with the specified float to see whether they are almost equal in reference to the specified
 * relative difference ratio. This algorithm compares the absolute difference in floats to the provided allowed ratio;
 * it doesn't work as well with very small numbers.
 *
 * @param that the comparison float
 * @param relDiff the allowed difference ratio
 * @return true if this.relativeDiff(that) <= relDiff
 * @see Float.relativeDifference
 */
fun Float.equalsRelDifference(that: Float, relDiff: Float = Float.FLT_EPSILON): Boolean {
    return this.relativeDifference(that) <= relDiff
}

/**
 * Calculates the relative difference between two floats.
 *
 * **See:** [C-FAQ - FAQ list Question 14.5 ](http://c-faq.com/fp/fpequal.html)
 *
 * @param that the comparison float
 * @return 0.0f if the two floats are equal, 1.0f if one of the floats equals 0.0f, else a ratio > 0.0f
 */
infix fun Float.relativeDifference(that: Float): Float {
    if (this == that) {
        return 0.0f
    }
    val absDiff = (this - that).absoluteValue
    val largestVal = max(this.absoluteValue, that.absoluteValue)
    return absDiff / largestVal
}

/**
 * Compares this value with the specified float to see whether they are almost equal in reference to the specified
 * ulp integer. This algorithm compares the actual difference in ulp to the provided allowed difference and works
 * best with numbers of the same sign.
 *
 * @param that the comparison float
 * @param ulp allowed units in the last place difference
 * @return true if this.ulp(that) <= ulp, false otherwise
 * @see Float.ulp
 */
fun Float.equalsUlp(that: Float, ulp: Int = 1): Boolean {
    if (this == that) {
        return true
    }
    if (this.sign != that.sign) {
        return false
    }
    return this.ulp(that).absoluteValue <= ulp
}

/**
 * Calculates the unit in the last place (ulp) difference between integer representations of two floats. The returned
 * value may overflow if the numbers are far enough apart.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 * **See:** [Wikipedia - Unit in the last place](https://en.wikipedia.org/wiki/Unit_in_the_last_place)
 *
 * @param that the comparison float
 * @return the ulp which may overflow
 */
infix fun Float.ulp(that: Float): Int {
    return this.toRawBits() - that.toRawBits()
}