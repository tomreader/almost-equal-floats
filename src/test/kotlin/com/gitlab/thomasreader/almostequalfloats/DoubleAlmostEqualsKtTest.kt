package com.gitlab.thomasreader.almostequalfloats

import org.junit.Test

import org.junit.Assert.*
import kotlin.math.nextDown
import kotlin.math.nextUp

class DoubleAlmostEqualsKtTest {

    @Test
    fun equalsEpsilonUlp() {
        assertEquals(true, 1.0.equalsEpsilonUlp(1.0, 0, 0.0))
        assertEquals(true, 1.0.equalsEpsilonUlp(1.0.nextUp(), 1, 0.0))
        assertEquals(true, 1.0.equalsEpsilonUlp(1.0.nextUp(), 0, Double.DBL_EPSILON))
        assertEquals(true, 0.0.equalsEpsilonUlp(-0.0, 0, 0.0))
        assertEquals(false, 1.0.equalsEpsilonUlp(1.0.nextDown(), 0, 0.0))
        assertEquals(false, 1.0.equalsEpsilonUlp(1.0.nextUp(), 0, 0.0))
        assertEquals(true, 0.0.nextUp().equalsEpsilonUlp(Double.DBL_EPSILON, 1, Double.DBL_EPSILON))
        assertEquals(false, 0.0.nextUp().equalsEpsilonUlp(Double.DBL_EPSILON, 1, 0.0))
    }

    @Test
    fun equalsEpsilonRelDiff() {
        assertEquals(true, 1.0.equalsEpsilonRelDiff(1.0, 0.0, 0.0))
        // epsilon of 0 doesn't matter as the relative difference is lte DBL_EPSILON
        assertEquals(true, 1.0.equalsEpsilonRelDiff(1.0.nextUp(), Double.DBL_EPSILON, 0.0))
        // relDiff of 0 doesn't matter as the difference is lt DBL_EPSILON
        assertEquals(true, 1.0.equalsEpsilonRelDiff(1.0.nextUp(), 0.0, Double.DBL_EPSILON))
        assertEquals(true, 0.0.equalsEpsilonRelDiff(-0.0, 0.0, 0.0))
        assertEquals(false, 1.0.equalsEpsilonRelDiff(1.0.nextDown(), 0.0, 0.0))
        assertEquals(false, 1.0.equalsEpsilonRelDiff(1.0.nextUp(), 0.0, 0.0))
        assertEquals(true, 0.0.nextUp().equalsEpsilonRelDiff(Double.DBL_EPSILON, Double.DBL_EPSILON, Double.DBL_EPSILON))
    }

    @Test
    fun equalsEpsilon() {
        assertEquals(true, 1.0.equalsEpsilon(1.0, 0.0))
        assertEquals(true, 1.0.equalsEpsilon(1.0.nextDown(), Double.DBL_EPSILON))
        assertEquals(true, 1.0.equalsEpsilon(1.0.nextUp(), Double.DBL_EPSILON))
        assertEquals(true, 0.0.equalsEpsilon(-0.0, 0.0))
        assertEquals(false, 1.0.equalsEpsilon(1.0.nextDown(), 0.0))
        assertEquals(false, 1.0.equalsEpsilon(1.0.nextUp(), 0.0))
        assertEquals(true, 0.0.nextUp().equalsEpsilon(Double.DBL_EPSILON, Double.DBL_EPSILON))
    }

    @Test
    fun equalsRelDifference() {
        // almost equal because relative difference of 1.0 and 1.0 = 0
        assertEquals(true, 1.0.equalsRelDifference(1.0, 0.0))
        // 0.0 and -0.0 equal
        assertEquals(true, 0.0.equalsRelDifference(-0.0, 0.0))
        // 1.0 and the next smallest float relative difference is lte DBL_EPSILON
        assertEquals(true, 1.0.equalsRelDifference(1.0.nextDown(), Double.DBL_EPSILON))
        // 1.0 and the next biggest float relative difference is lte DBL_EPSILON
        assertEquals(true, 1.0.equalsRelDifference(1.0.nextUp(), Double.DBL_EPSILON))
        // these same comparisons are now false with a relative difference of the smallest float (subnormal)
        assertEquals(false, 1.0.equalsRelDifference(1.0.nextDown(), Double.MIN_VALUE))
        assertEquals(false, 1.0.equalsRelDifference(1.0.nextUp(), Double.MIN_VALUE))
        assertEquals(false, 0.0.nextUp().equalsRelDifference(Double.DBL_EPSILON, Double.DBL_EPSILON))
        assertEquals(true, 0.0.nextUp().equalsRelDifference(Double.DBL_EPSILON, 1.0))
    }

    @Test
    fun relativeDifference() {
        // equal so 0 difference
        assertEquals(0.0, 0.0.relativeDifference(-0.0), 0.0)
        assertEquals(0.0, 1.0.relativeDifference(1.0), 0.0)
        // order of floats makes no difference for this algorithm
        assertEquals(1.0 / 2.0, 1.0.relativeDifference(2.0), 0.0)
        assertEquals(1.0 / 2.0, 2.0.relativeDifference(1.0), 0.0)
        // involves zero and a non zero value, so expected == 1.0
        assertEquals(1.0, 0.0.relativeDifference(1.4298789), 0.0)
    }

    @Test
    fun equalsUlp() {
        assertEquals(true, 1.0.equalsUlp(1.0, 0))
        // 1.0.nextDown() is 1 ulp away from 1.0
        assertEquals(true, 1.0.equalsUlp(1.0.nextDown(), 1))
        // 1.0.nextUp() is 1 ulp away from 1.0
        assertEquals(true, 1.0.equalsUlp(1.0.nextUp(), 1))
        // -0.0 == 0.0 despite being -2147483648 ulp away
        assertEquals(true, 0.0.equalsUlp(-0.0, 0))
        assertEquals(false, 1.0.equalsUlp(1.0.nextDown(), 0))
        assertEquals(false, 1.0.equalsUlp(1.0.nextUp(), 0))
        // smallest subnormal and smallest normal are ~4500+ trillion ulp away despite being closer in value
        // than 1.0 and 1.00000011920929f which are 1 ulp away
        assertEquals(false, Double.MIN_VALUE.equalsUlp(2.2250738585072014E-308, 2000))
        assertEquals(true, Double.MIN_VALUE.equalsUlp(2.2250738585072014E-308, 4_503_599_627_370_495))
    }

    @Test
    fun ulp() {
        assertEquals(0, 1.0.ulp(1.0))
        // this bigger than that so positive
        assertEquals(1, 1.0.ulp(1.0.nextDown()))
        // this smaller than that so negative
        assertEquals(-1, 1.0.ulp(1.0.nextUp()))
        // this in bits == 0, -0.0 in bits = Long.MIN_VALUE, this overflows back to Long.MIN_VALUE
        assertEquals(Long.MIN_VALUE, 0.0.ulp(-0.0))
        // this one doesn't overflow as -(Long.MIN_VALUE - 1) == Long.MAX_VALUE
        assertEquals(Long.MAX_VALUE, 0.0.ulp((-0.0).nextDown()))
    }
}